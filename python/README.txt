venvrun: run in venv without source and deactivate

use:
	mkdir project && cd project
	<copy to dir venvrun>
	python -m venv env
	source env/bin/activate
	edit requirements.txt
	pip install -r requirements.txt
	deactivate
	./venvrun pip freeze
