#!/bin/bash
#

NAME="${NAME:-vm0}"
ARCH="${ARCH:-i386}"

CPU="${CPU:-coreduo}"

SMP="${SMP:-1}"
MEM="${MEM:-1024}"
VGA="${VGA:-std}"
NET="${NET:-nic -net user,hostfwd=tcp::2222-:22}"
USB="${USB:-}"

AUDIO="${AUDIO:-id=pa,driver=pa}" # -audiodev id=alsa,driver=alsa

DSIZE="${DSIZE:-32G}"
DNAME="${DNAME:-disk}"
DEXT="${DEXT:-qcow2}"
DISK="${DISK:-$PWD/${DNAME}.${DEXT}}"

OPTS="${OPTS:---enable-kvm}"

log(){
	printf " :: $1\n"
}

p_tree(){
	cat << EOF
Hardware used:
  NAME:  ${NAME}
  ARCH:  ${ARCH}
  CPU:   ${CPU}
  SMP:   ${SMP}
  MEM:   ${MEM}
  VGA:   ${VGA}
  NET:   ${NET}
  USB:   ${USB}
  AUDIO: ${AUDIO}

Hardrive disk:
  SIZE:  ${DSIZE}
  DISK:  ${DISK}

More options:
  OPTS:  ${OPTS}

EOF
}

c_disk(){
	if [ ! -f "${DISK}" ]; then
		log "Create disk"
		qemu-img create -f ${DEXT} ${DISK} ${DSIZE}
	fi
}

main(){
	p_tree
	c_disk "$@"

	log "Run '${NAME}':"
	exec qemu-system-${ARCH} \
		-boot menu=on \
		-name "${NAME}" \
		-cpu ${CPU} \
		-smp ${SMP} \
		-m ${MEM} \
		-vga ${VGA} \
		-net ${NET} \
		-usb ${USB} \
		-audiodev ${AUDIO} \
		-device usb-ehci,id=ehci \
		-device usb-tablet,bus=usb-bus.0 \
		-drive id=disk,file=${DISK},if=none \
		-device ahci,id=ahci \
		-device ide-hd,drive=disk,bus=ahci.0 \
		${OPTS} \
		"$@"

}

main "$@"
