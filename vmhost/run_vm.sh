#!/bin/bash
#

set -e

QNAME="${QNAME:-vm0}"
QARCH="${QARCH:-x86_64}"
MACHINE="pc"

if [ "$(LC_ALL=C lscpu | grep "Model name" | grep Intel)" != "" ]; then
        CPU="${CPU:-core2duo}"
else
        CPU="${CPU:-Opteron_G1-v1}"
fi

MEM="${MEM:-2048}"
SMP="${SMP:-2}"
VGA="${VGA:-std}" # std qxl
NET="nic -net user,hostfwd=tcp::2222-:22"
USB="-device usb-tablet,bus=usb-bus.0"
SND="-device intel-hda -device hda-duplex"

#NOPT="-display sdl"

ISO_FILE="`find . -type f -name '*.iso'`"

if [ "${ISO_FILE}" != "" ]; then
	GOPT="${ISO_FILE}"
fi

# drive
DNAME="disk"
D_EXT="qcow2"
DSIZE="32G"
HDRIVE="${DNAME}.${D_EXT}"

SH_DIR="${PWD}/shared"

_nopt(){
	case "${QARCH}" in
		x86_64) NOPT="-enable-kvm ${NOPT}"
	esac
}

create_hdd(){
	if [ ! -f "${HDRIVE}" ]; then
		qemu-img create -f ${D_EXT} ${HDRIVE} ${DSIZE} || exit 1
	fi
}

print_sys(){
cat << _EOF
------------------------------------
Virtual Machine specs:

ARCH: ${QARCH}
CPU:  ${CPU}
MEM:  ${MEM}
SMP:  ${SMP}
VGA:  ${VGA}
SND:  ${SND}
HDD:  ${HDRIVE}

NOPT: ${NOPT}
GOPT: ${GOPT}
------------------------------------
_EOF
}

main(){
	_nopt "$@"
	if [ -d "${SH_DIR}" ]; then
		printf " :: Shared directory '${SH_DIR}', and address '///10.0.2.4/qemu'. \n"
	fi

	print_sys "$@"

	create_hdd "$@"
	qemu-system-${QARCH} ${NOPT} \
		-name "${QNAME}" \
		-M ${MACHINE} \
		-boot menu=on \
		-cpu ${CPU} \
		-smp ${SMP} \
		-m ${MEM} \
		-vga ${VGA} \
		-net ${NET} $([ -d "$SH_DIR" ] && echo -net user,smb=${SH_DIR}) \
		${SND} \
		-usb ${USB} \
		-drive id=disk,file="${DNAME}.${D_EXT}",if=none \
		-device ahci,id=ahci \
		-device ide-hd,drive=disk,bus=ahci.0 ${GOPT} "$@"
}

main "$@"
