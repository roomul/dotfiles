#!/bin/bash
#

log(){
	printf " :: $1\n"
}

log2(){
	printf " -> $1\n"
}

err(){
	log "ERROR: $1"
	exit 1
}

create_hdd(){
	if [ -f "${DISK}" ]; then
		log2 "Disk '${DISK}' found"
		exit 0
	else
		log "Disk not found, create '$DISK'..."
		qemu-img create -f ${DEXT} ${DISK} ${DSIZE} || exit 1
	fi
}

main(){
	case "$1" in
		c|create_hdd|-c|--create_hdd)
			create_hdd
			exit 0
			;;
	esac
	
	[[ -f "${DISK}" ]] || create_hdd

	cat << EOF

  :: Using specs:
  NAME:     ${QNAME}
  ARCH:     ${ARCH}
  CPU:      ${CPU}
  SMP:      ${SMP}
  MEM:      ${MEM}
  NET:      ${NET}
  VGA:      ${VGA}
  USB:      ${USB}
  DISK:     ${DISK}
  AUDIO:    ${AUDIO}
  SSH_PORT: ${SSH_PORT}

EOF

	exec qemu-system-${ARCH} \
		-boot menu=on \
		-machine q35,smm=on \
		-name ${QNAME} \
		-cpu ${CPU} \
		-smp ${SMP} \
		-m ${MEM} \
		-enable-kvm \
		-net ${NET} \
		-vga ${VGA} \
		-usb ${USB} \
		-device usb-tablet,bus=usb-bus.0 \
		-audiodev ${AUDIO} \
		-drive if=pflash,format=raw,file="${OVMF_CODE}",readonly=on \
		-drive file="${DISK}",if=virtio \
		"$@"
}

DNAME="${DNAME:-disk}"
DSIZE="${DSIZE:-64G}"
DEXT="${DEXT:-qcow2}"
DISK="${DISK:-${DNAME}.${DEXT}}"

SSH_PORT="${SSH_PORT:-2222}"

QNAME="${QNAME:-vm0}"
ARCH="${ARCH:-x86_64}"
CPU="${CPU:-core2duo}"
SMP="${SMP:-2}"
MEM="${MEM:-2048}"
VGA="${VGA:-qxl}" # virtio std cirrus qxl vmware
NET="${NET:-nic,model=virtio -net user,hostfwd=tcp::${SSH_PORT}-:22}"
USB="${USB:-}"
AUDIO="${AUDIO:-id=pa,driver=pa}"

OVMF_CODE="${OVMF_CODE:-/usr/share/edk2-ovmf/OVMF_CODE.fd}"
OVMF_VARS="${OVMF_VARS:-/usr/share/edk2-ovmf/OVMF_VARS.fd}"

main "$@"
