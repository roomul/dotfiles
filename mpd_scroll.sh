#!/usr/bin/env bash
#

if ! mpc >/dev/null 2>&1; then
		echo "Server offline"
		exit 1
elif mpc status | grep -q playing; then
		#( mpc current | zscroll -l 10 -d 0.1 -n 1 ) &
		( zscroll --before-text "♪ x" --delay 0.3 \
		--match-command "mpc status" \
		--match-text "playing" "--before-text ' '" \
		--match-text "paused" "--before-text ' ' --scroll 0" \
		--update-check true "mpc current") &
else
		echo "Not playing"
fi

mpc idle>/dev/null
exit 0
