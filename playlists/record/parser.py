#!/usr/bin/env python3

import os
import json
import requests

from pathlib import Path

url = "https://radiorecord.ru/api/stations"
file = "stations"

pathFile = Path(file)

if not pathFile.is_file():
    data = requests.get(url, allow_redirects=True)
    open(file, 'wb').write(data.content)

api = json.load(open(file, 'r'))

print("#EXTM3U")

for item in api['result']['stations']:
    print()
    print("#EXTINF:0,", str(item['title']))
    print(item['stream_320'])
