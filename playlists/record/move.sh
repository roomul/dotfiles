#!/bin/sh

DATA=$(date +'%d.%m.%Y')

find . -path ./old -prune -o -name '*.m3u' -print -exec mv -vt ./old/ {} +
find . -name 'stations' -exec mv -v {} ./old/{}_${DATA} \; 
