#!/bin/bash
#

log(){
	printf " -> $1\n"
}

err(){
	log "ERROR: $1"
	exit 1
}

trapi(){
	printf "\n"
	rm -vf $PWD/archive/${DIR}-${DATA}.tar.*
	err "Trapping, exit..."
}

pack(){
	if [ -f "$PWD/archive/${DIR}-${DATA}.tar.${Z}" ]; then
		log "Found,skeep..."
	else
		log "Create backup: `basename $PWD/archive/${DIR}-${DATA}.tar.${Z}`"
		sleep 2s
		if [ -f "/usr/bin/pv" ]; then
			tar -cpf - ${DIR}/ | pv | ${ZP} -c - > $PWD/archive/${DIR}-${DATA}.tar.${Z}
		else
			tar -cvpf - ${DIR}/ | ${ZP} -c - > $PWD/archive/${DIR}-${DATA}.tar.${Z}
		fi
		sync
		log "Size: $(du -h $PWD/archive/${DIR}-${DATA}.tar.${Z} | awk '{ print $1 }' )"
	fi
}

main(){
	set -e
	trap "trapi" ERR INT

	export ZSTD_OPT="-19v"
	export XZ_OPT="-9"

	Z=${Z:-xz}

	case ${Z} in
		zst)
			ZP="zstd" ;;
		xz)
			ZP="xz" ;;
	esac

	[[ "$1" != "" ]] || err "Not set backup directory."
	[[ -d "$PWD/archive" ]] || install -dm755 $PWD/archive
	[[ -f "$PWD/repos" ]] || err "Not found 'repos' file."

	DATA=$(date +"%d.%m.%Y")

	for repo in $(cat $PWD/repos) ; do
		local DIR=$(basename $repo)
		if [ -d "$DIR" ]; then
			case "$1" in
				${DIR})
					pack "$@"
					exit 0
					;;
			esac
		fi
	done

	sync
	exit 0
}

main "$@"
