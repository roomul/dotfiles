#!/bin/bash
#

log(){
	printf " -> $1\n"
}

err(){
	log "ERROR: $1"
	exit 1
}

trapi(){
	rm -f $PWD/archive/${DIR}-${DATA}.tar.xz
	err "Trapping, exit..."
}

main(){
	set -e
	trap "trapi" INT ERR

	[[ -d "$PWD/archive" ]] || install -dm755 $PWD/archive
	[[ -f "$PWD/repos" ]] || err "Not found 'repos' file."

	DATA=$(date +"%d.%m.%Y")

	for repo in $(cat $PWD/repos) ; do
		local DIR=$(basename $repo)
		if [ -d "$DIR" ]; then
			if [ -f "$PWD/archive/${DIR}-${DATA}.tar.zst" ]; then
				log "Found,skeep..."
			else
				log "Create backup: `basename $PWD/archive/${DIR}-${DATA}.tar.zst`"
				tar -cpf - ${DIR}/ | pv | zstd -9 -c - > $PWD/archive/${DIR}-${DATA}.tar.zst
				log "Size: $(du -h $PWD/archive/${DIR}-${DATA}.tar.zst | awk '{ print $1 }' )"
				sync
				printf "\n"
			fi
		fi
	done

	sync
	exit 0
}

main "$@"
