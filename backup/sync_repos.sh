#!/bin/bash
#

log(){
	printf " -> $1\n"
}

err(){
	log "ERROR: $1"
	exit 1
}

main(){
	[[ -f "$PWD/repos" ]] || err "Not found 'repos' file."

	for repo in $(cat $PWD/repos) ; do
		local DIR=$(basename $repo)
		if [ -d "$DIR" ]; then
						cd $DIR >/dev/null
						log "Update - $DIR"
						git pull
						sync
						cd - >/dev/null
		else
						log "Clone - $DIR"
						git clone --depth=1 $repo
						sync
		fi
	done

	sync
	exit 0
}

main "$@"
