#!/bin/sh -e
#

USER="roomul"
PASS="$1"

REPOS="community ports repo"

for repo in $REPOS ; do
	if [ -d "$repo" ]; then
		cd $repo
			git config pull.rebase false
			git pull
		cd ..
	else
		git clone https://${USER}:${PASS}@gitlab.com/${USER}/$repo
		sync
	fi
done

sync
exit 0
