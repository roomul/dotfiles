" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" https://github.com/junegunn/vim-plug
"

set encoding=utf-8

set nocp
set nu
set ls=2
set noshowmode
set tabstop=4
set expandtab

set noswapfile
set nobackup

" fix start replace mod disable by default
set t_u7=

if !isdirectory("~/.vim/plugged")
	call plug#begin()
		Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }
		Plug 'vim-airline/vim-airline'
		Plug 'vim-airline/vim-airline-themes'
        Plug 'ryanoasis/vim-devicons'
	call plug#end()
endif

if !isdirectory("~/.vim/plugged/nerdtree")
	nnoremap <leader>n :NERDTreeFocus<CR>
	nnoremap <C-n> :NERDTree<CR>
	nnoremap <C-t> :NERDTreeToggle<CR>
	nnoremap <C-f> :NERDTreeFind<CR>

	let g:NERDTreeDirArrowExpandable = '?'
	let g:NERDTreeDirArrowCollapsible = '?'
endif


if !isdirectory("~/.vim/plugged/vim-airline")
	let g:airline_theme='minimalist'

    let g:airline_powerline_fonts = 1
    
    if !exists('g:airline_symbols')
        let g:airline_symbols = {}
    endif

    " unicode symbols
    let g:airline_left_sep = '»'
    let g:airline_left_sep = '▶'
    let g:airline_right_sep = '«'
    let g:airline_right_sep = '◀'
    let g:airline_symbols.linenr = '␊'
    let g:airline_symbols.linenr = '␤'
    let g:airline_symbols.linenr = '¶'
    let g:airline_symbols.branch = '⎇'
    let g:airline_symbols.paste = 'ρ'
    let g:airline_symbols.paste = 'Þ'
    let g:airline_symbols.paste = '∥'
    let g:airline_symbols.whitespace = 'Ξ'

    " airline symbols
    let g:airline_left_sep = ''
    let g:airline_left_alt_sep = ''
    let g:airline_right_sep = ''
    let g:airline_right_alt_sep = ''
    let g:airline_symbols.branch = ''
    let g:airline_symbols.readonly = ''
    let g:airline_symbols.linenr = ''
endif

filetype indent on

if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

syntax on
" color elflord

