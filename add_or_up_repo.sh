#!/bin/sh -e

URL="https://codeberg.org/roomul"
REPOS="repo community dotfiles ports"
DIRS=$(ls -A)

log(){
	printf " :: $1 \n"
}

main(){
	for repo in $REPOS; do
		if [ ! -d "$repo" ]; then
			log "$repo - clone"
			git clone $URL/$repo
		fi
	done

	for dir in $DIRS; do
		if [ -d "$dir" ]; then
			if [ -d "$dir/.git" ]; then
				cd $dir
					log "Up: $dir"
					git config pull.rebase false
					git pull 
				cd - >/dev/null
			fi
		fi
	done

	sync
	exit 0
}

main "$@"
