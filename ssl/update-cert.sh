#!/bin/sh -e
#

URL="https://curl.se/ca/cacert.pem"
PEM="cert.pem"

main(){
	if [ "`id -u`" != 0 ]; then
		printf " -> ERROR: You are not a root.\n"
		exit 1
	fi

	cd /etc/ssl
	wget --no-check-certificate $URL -O $PEM

	sync
	exit 0
}

main "$@"
